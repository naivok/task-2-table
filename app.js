var $header = $('.header'),
    $settings = $('.settings'),
    $modal = $('.modal'),
    $modalOverlay = $('.modal__overlay'),
    $openModalBtn = $('.subheader__icon');

$header.click(function() {
    $(this).toggleClass('header__hiden-settings');
    $settings.slideToggle();
});

$openModalBtn.click(function() {
    $modal.fadeIn();
});
$modalOverlay.click(function() {
    $modal.fadeOut();
});

    var $startDateElem = $('#startDate');
    var $endDateElem = $('#endDate');



    $startDateElem.datepicker();
    $endDateElem.datepicker();

    var today = new Date();
    var yesterday = new Date(Date.now() - 86400000);

    $startDateElem.datepicker( 'setDate', yesterday );
    $endDateElem.datepicker( 'setDate', today );

    $startDateElem.focusin(function() {
        $(this).datepicker('show');
    });

    $endDateElem.focusin(function() {
        $(this).datepicker('show');
    });

    var $showDataBtn = $('#showData');

    var HomeModel = function() {
        this.dataLoaded = ko.observable(false);
        this.rows = ko.observableArray([]);
    };

    var model = new HomeModel();
    ko.applyBindings(model);
    $showDataBtn.click(function() {
        $.getJSON('data.json')
        .done(function( data ) {
            model.rows([]);
            for (var x in data) {
                if ((new Date($startDateElem.datepicker( "getDate" )).getTime() <= new Date(data[x].StartDate).getTime())
                    && (new Date(data[x].StartDate).getTime() <= new Date($endDateElem.datepicker( "getDate" )).getTime())) {
                    model.rows.push(data[x]);
                }
            }
            model.dataLoaded(true);
        });
    });

