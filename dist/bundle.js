/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 0);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ (function(module, exports, __webpack_require__) {

__webpack_require__(1);
module.exports = __webpack_require__(2);


/***/ }),
/* 1 */
/***/ (function(module, exports) {

var $header = $('.header'),
    $settings = $('.settings'),
    $modal = $('.modal'),
    $modalOverlay = $('.modal__overlay'),
    $openModalBtn = $('.subheader__icon');

$header.click(function() {
    $(this).toggleClass('header__hiden-settings');
    $settings.slideToggle();
});

$openModalBtn.click(function() {
    $modal.fadeIn();
});
$modalOverlay.click(function() {
    $modal.fadeOut();
});

    var $startDateElem = $('#startDate');
    var $endDateElem = $('#endDate');



    $startDateElem.datepicker();
    $endDateElem.datepicker();

    var today = new Date();
    var yesterday = new Date(Date.now() - 86400000);

    $startDateElem.datepicker( 'setDate', yesterday );
    $endDateElem.datepicker( 'setDate', today );

    $startDateElem.focusin(function() {
        $(this).datepicker('show');
    });

    $endDateElem.focusin(function() {
        $(this).datepicker('show');
    });

    var $showDataBtn = $('#showData');

    var HomeModel = function() {
        this.dataLoaded = ko.observable(false);
        this.rows = ko.observableArray([]);
    };

    var model = new HomeModel();
    ko.applyBindings(model);
    $showDataBtn.click(function() {
        $.getJSON('data.json')
        .done(function( data ) {
            model.rows([]);
            for (var x in data) {
                if ((new Date($startDateElem.datepicker( "getDate" )).getTime() <= new Date(data[x].StartDate).getTime())
                    && (new Date(data[x].StartDate).getTime() <= new Date($endDateElem.datepicker( "getDate" )).getTime())) {
                    model.rows.push(data[x]);
                }
            }
            model.dataLoaded(true);
        });
    });



/***/ }),
/* 2 */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ })
/******/ ]);