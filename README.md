# Task 2

Add some datepickers for these inputs (for example, [http://api.jqueryui.com/datepicker/](http://api.jqueryui.com/datepicker/)).
Initial date for Start date is yesterday.
Initial date for End date is today.
Data should be loaded using jQuery (create a static page with mock data from data.json) when you click on refresh button.
Call to that page should contain startDate and endDate as parameters.
Create a table and style it.
Use warning-WF-square.png and check-WF,png for status column.
Data should be bound to that table using Knockout.js
Table should be visible only after data has been loaded.
Only 5 items should be visible but all off them should be present in Knockout viewmodel.

[Click here to view online](https://naivok.gitlab.io/task-2-table/table.html)

To see the results, select Start Date 12/03/2017
